package com.seb.flashcards

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.get
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var mainViewModel: MainViewModel
    
    private var pinyinVisibility = false
    private val PINYIN_ITEM_INDEX = 1


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initializeModels()
    }

    override fun onClick(v: View) {
        mainViewModel.onOptionClick(v.id)
    }

    private fun initializeModels() {
        mainViewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)

        setListenersOnButtons()

        mainViewModel.getPoints().observe(this, Observer { pointsLabel.text = it.toString() })

        mainViewModel.getButtonTextChinese().observe(this, Observer { setButtonsText(it) })

        mainViewModel.getSolutionEnglishText().observe(this, Observer { setQuestionText(it) })

        mainViewModel.getButtonsVisibilities().observe(this, Observer { setVisibilities(it) })

        mainViewModel.getPinyinPair().observe(this, Observer {

            setPinyin(it)

            // TODO poprawic pobieranie tej zmiennej z ViewModelu

            pinyinVisibility = it.second!! // ..............
        })

    }

    private fun setVisibilities(visibilityList: IntArray) {
        option0.visibility = visibilityList[0]
        option1.visibility = visibilityList[1]
        option2.visibility = visibilityList[2]
        option3.visibility = visibilityList[3]
    }

    private fun setButtonsText(chineseList: List<String>) {
        option0.text = chineseList[0]
        option1.text = chineseList[1]
        option2.text = chineseList[2]
        option3.text = chineseList[3]
    }


    private fun setQuestionText(solutionEnglish: String) {
        questionWordPrimary.text = solutionEnglish
    }

    private fun setPinyin(pinyin: Pair<String, Boolean?>) {
        if (pinyin.second!!) {
            questionWordSecondary.text = pinyin.first
        } else {
            questionWordSecondary.text = null
        }
    }

    private fun setListenersOnButtons() {
        option0.setOnClickListener(this)
        option1.setOnClickListener(this)
        option2.setOnClickListener(this)
        option3.setOnClickListener(this)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val menuInflater = menuInflater
        menuInflater.inflate(R.menu.reset_menu, menu)
        menu.getItem(PINYIN_ITEM_INDEX).isChecked = pinyinVisibility // set checkBox state checked/unchecked
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.reset_button -> {
                mainViewModel.onResetButtonClicked()
                return true
            }

            R.id.show_pinyin -> {
                mainViewModel.setPinyinVisibility()
                item.isChecked = pinyinVisibility
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
