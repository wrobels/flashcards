package com.seb.flashcards.data

import com.seb.flashcards.entities.Flashcard

interface Repository {
    fun getRandomFlashcard(): Flashcard
}
