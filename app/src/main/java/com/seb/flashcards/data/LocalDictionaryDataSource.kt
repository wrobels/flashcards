package com.seb.flashcards.data

import android.content.Context
import com.seb.flashcards.R
import com.seb.flashcards.entities.Word

class LocalDictionaryDataSource(context: Context) : DataSource {
    override val dictionary = context.resources.getStringArray(R.array.dictionary)
            .map { it.split(LOCAL_DICTIONARY_DELIMITER) }
            .map { it.map(String::trim) }
            .map { it.toWord() }

    private fun List<String>.toWord(): Word {
        check(size == 3)
        return Word(get(0), get(1), get(2))
    }

    companion object {
        private const val LOCAL_DICTIONARY_DELIMITER = "|"
    }
}
