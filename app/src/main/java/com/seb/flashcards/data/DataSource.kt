package com.seb.flashcards.data

import com.seb.flashcards.entities.Word

interface DataSource {
    val dictionary: List<Word>
}
