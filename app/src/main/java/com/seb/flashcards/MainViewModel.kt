package com.seb.flashcards

import android.app.Application
import android.view.MenuItem
import android.view.View
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.seb.flashcards.data.DictionaryRepository
import com.seb.flashcards.data.LocalDictionaryDataSource
import com.seb.flashcards.data.Repository
import com.seb.flashcards.entities.Flashcard
import com.seb.flashcards.entities.Word

class MainViewModel(application: Application) : AndroidViewModel(application) {
    private val repository: Repository = DictionaryRepository(LocalDictionaryDataSource(application))

    private var flashcard = MutableLiveData<Flashcard>().apply { value = repository.getRandomFlashcard() }
    private var points = MutableLiveData<Int>().apply { value = 0 }
    private var pointsToEarn = Flashcard.POINTS
    private var pinyinVisibility = MutableLiveData<Boolean>().apply { value = false }
    // private var pinyinString = MutableLiveData<String>().apply { value = flashcard.value?.solution?.pinyin}
    private var wordToGuess: Word? = flashcard.value?.solution

    private var chineseList = MutableLiveData<List<String>>().apply {
        value = flashcard.value?.answers?.map { it.chinese }
    }

    private var solutionEnglish: MutableLiveData<String> = MutableLiveData(String()).apply {
        value = flashcard.value?.solution?.english
    }

    private var visibilities = MutableLiveData<IntArray>().apply {
        value = defaultVisibilities()
    }

    private var pinyinWord: MutableLiveData<Pair<String, Boolean?>> =
        MutableLiveData<Pair<String, Boolean?>>().apply {
            value = Pair(flashcard.value?.solution!!.pinyin, pinyinVisibility.value)
        }

    private var pinyinItem: LiveData<MenuItem> = MutableLiveData<MenuItem>().apply { value?.isChecked = false }

    private fun defaultVisibilities(): IntArray {
        return IntArray(4) { View.VISIBLE }
    }

    fun getPinyinPair(): LiveData<Pair<String, Boolean?>> {
        return pinyinWord
    }

    fun getButtonTextChinese(): LiveData<List<String>> {
        return chineseList
    }

    fun getSolutionEnglishText(): LiveData<String> {
        return solutionEnglish
    }

    private fun resetVisibilities() {
        visibilities.postValue(defaultVisibilities())
    }

    fun getButtonsVisibilities(): LiveData<IntArray> {
        return visibilities
    }

    fun getPoints(): LiveData<Int> {
        return points
    }

    fun setPinyinVisibility() {
        pinyinVisibility.postValue(!pinyinVisibility.value!!)
    }

    private fun generateNewFlashcard() {
        flashcard.postValue(repository.getRandomFlashcard())
        pointsToEarn = Flashcard.POINTS
        visibilities.postValue(defaultVisibilities())
        solutionEnglish.postValue(flashcard.value?.solution?.english)
        chineseList.postValue(flashcard.value?.answers?.map { it.chinese })
        //  pinyinString.postValue(flashcard.value?.solution?.pinyin)
        pinyinWord.postValue(Pair(flashcard.value?.solution!!.pinyin, pinyinVisibility.value))

    }

    private fun resetPoints() {
        points.postValue(0)
    }

    fun onResetButtonClicked() {
        resetPoints()
        resetVisibilities()
    }

    fun onOptionClick(option: Int) {
        when (option) {
            R.id.option0 -> wordToGuess = flashcard.value?.answers?.get(0)
            R.id.option1 -> wordToGuess = flashcard.value?.answers?.get(1)
            R.id.option2 -> wordToGuess = flashcard.value?.answers?.get(2)
            R.id.option3 -> wordToGuess = flashcard.value?.answers?.get(3)
        }

        wordToGuess?.let { compareChosenWordWithSolution(it) }
    }

    private fun compareChosenWordWithSolution(chosenWord: Word) {

        if (chosenWord != flashcard.value!!.solution) {

            val index = flashcard.value!!.answers.indexOf(chosenWord)

            if (pointsToEarn >= 1) {
                pointsToEarn -= 1
            } else {
                pointsToEarn = 0
            }

            val visibilitiesTmp = MutableLiveData<IntArray>().apply { value = visibilities.value }

            visibilitiesTmp.value?.set(index, View.INVISIBLE)

            visibilities.postValue(visibilitiesTmp.value)

        } else {
            if (pointsToEarn == 0) {
                points.value = 0
            } else {
                points.value = points.value?.plus(pointsToEarn)
            }
            generateNewFlashcard()
        }
    }
}