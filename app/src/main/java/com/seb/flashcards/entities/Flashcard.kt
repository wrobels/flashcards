package com.seb.flashcards.entities

data class Flashcard(
    val solution: Word,
    val answers: List<Word>
) {

    companion object {
        const val ANSWERS_AMOUNT = 4
        const val POINTS = 3
    }
}
