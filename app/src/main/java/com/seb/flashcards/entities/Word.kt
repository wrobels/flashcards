package com.seb.flashcards.entities

data class Word(
    val chinese: String,
    val pinyin: String,
    val english: String
)
