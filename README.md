# flashcards

A simple game in kotlin.

A word in english appears on the top of the screen and user has to choose one from four options which corresponds to it. 

The amount of points, which user can gain for question, is 3. For every wrong answer available points are decreased by 1. 
After three wrong answers in a row sum of points resets to 0.

#### Architecture
Application based on **MVVM** pattern.


#### Screenshots

##### Screen 1

<img src="./screens/scr-1.jpg" width="150"> 

##### Screen 2

<img src="./screens/scr-2.jpg" width="150">